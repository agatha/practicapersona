class Persona:
    def __init__(self, nombre, apellidos, nacimiento, DNI):
        self.nombre = nombre
        self.apellidos = apellidos
        self.nacimiento = nacimiento
        self.DNI = DNI

    def setNombre(self, nombre): 
        self._nombre = nombre

    def getNombre(self): 
        return self._nombre 

    def setApellidos(self, apellidos): 
        self._apellidos = apellidos

    def getApellidos(self): 
        return self._apellidos

    def setNacimiento(self, nacimiento): 
        self._nacimiento = nacimiento

    def getNacimiento(self): 
        return self._nacimiento

    def setDNI(self, DNI): 
        self._DNI = DNI
    
    def setNacimiento(self): 
         return self._DNI

    def __str__(self):
        return 'El paciente {nombre} {apellidos}, con fecha de nacimiento: {nacimiento} y con DNI: {DNI}'.format(nombre= self.nombre, apellidos= self.apellidos, nacimiento= self.nacimiento, DNI= self.DNI)

    
class Paciente(Persona):
    def __init__(self, nombre, apellidos, nacimiento, DNI, historial_clinico):
        super().__init__(nombre, apellidos, nacimiento, DNI)
        self.historial_clinico = historial_clinico
    def VerHistorialClinico(self):
        return self.historial_clinico

    def __str__(self):
        return 'El paciente {nombre} {apellidos}, con fecha de nacimiento: {nacimiento} , con DNI: {DNI} y cuyo historial clínico es: {historial_clinico}'.format(nombre= self.nombre, apellidos= self.apellidos, nacimiento= self.nacimiento, DNI= self.DNI, historial_clinico= self.historial_clinico)


class Medico(Persona):
    def __init__(self, nombre, apellidos, nacimiento, DNI, especialidad, citas):
        super().__init__(nombre, apellidos, nacimiento, DNI)
        self.especialidad = especialidad
        self.citas = citas
    def ConsultarAgenda(self):
        return (self.especialidad, self.citas)

    def __str__(self):
        return 'El paciente {nombre} {apellidos}, con fecha de nacimiento: {nacimiento}, DNI: {DNI} , tratado en la/s especialidad/es: {especialidad} y con cita/s: {cita}'.format(nombre= self.nombre, apellidos= self.apellidos, nacimiento= self.nacimiento, DNI= self.DNI, especialidad= self.especialidad, cita= self.cita)

paciente1 = Paciente('Juan', 'López Martín', '28/04/2005', '12345678A', 'Cáncer')
print(paciente1)
medico1 = Medico('Luis', 'Crespo Pérez', '28/04/1999', '12345678B', 'Oncología', '9/03/2024')
print(medico1)

if __name__ == '__main__':
    unittest.mai()
