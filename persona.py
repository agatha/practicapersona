class Persona:
    def __init__(self, nombre, apellidos, nacimiento, DNI):
        self.nombre = nombre
        self.apellidos = apellidos
        self.nacimiento = nacimiento
        self.DNI = DNI

    def __str__(self):
        return "El paciente {nombre} {apellidos}, con fecha de nacimiento {nacimiento} tiene DNI: {DNI}".format(nombre= self.nombre, apellidos= self.apellidos, nacimiento= self.nacimiento, DNI= self.DNI)

    def setNombre(self, nombre): 
        self._nombre = nombre

    def getNombre(self): 
        return self._nombre 

    def setApellidos(self, apellidos): 
        self._apellidos = apellidos

    def getApellidos(self): 
        return self._apellidos

    def setNacimiento(self, nacimiento): 
        self._nacimiento = nacimiento

    def getNacimiento(self): 
        return self._nacimiento

    def setDNI(self, DNI): 
        self._DNI = DNI
    
    def getDNI(self): 
         return self._DNI

    def __str__(self):
        return '{nombre} {apellidos}, nacido el {nacimiento} y con {DNI}'.format(nombre= self.nombre, apellidos= self.apellidos, nacimiento= self.nacimiento, DNI= self.DNI)  
class Paciente(Persona):
    def __init__(self, nombre, apellidos, nacimiento, DNI, historial_clinico):
        super().__init__(nombre, apellidos, nacimiento, DNI)
        self.historial_clinico = historial_clinico
    def VerHistorialClinico(self):
        return self.historial_clinico
class Medico(Persona):
    def __init__(self, nombre, apellidos, nacimiento, DNI, especialidad, citas):
        super().__init__(nombre, apellidos, nacimiento, DNI)
        self.especialidad = especialidad
        self.citas = citas
    def ConsultarAgenda(self):
        return (self.especialidad, self.citas)

